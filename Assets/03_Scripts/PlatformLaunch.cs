﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformLaunch : MonoBehaviour
{
    ///launchStrenght * strengthPercent = currentStrength
    [Tooltip("The initial launch strength of the platform; diminishes as the platform animation progresses")]
    public float launchStrength = 12.0f;
    float currentStrength;
    [HideInInspector, Tooltip("Percentage of launchStrength that is set to currenStrength; controlled by the animator")]
    public float strengthPercent = 1.0f;

    void Start()
    {
        
    }

    void Update()
    {
        currentStrength = launchStrength * strengthPercent;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && strengthPercent > 0)
        {
            other.transform.parent = gameObject.transform;
            Vector3 launchForce = Quaternion.LookRotation(transform.forward, transform.up) * new Vector3(0, 0, currentStrength);
            launchForce.y = Mathf.Clamp(launchForce.y, -8.0f, 6.5f);
            other.gameObject.GetComponent<PlayerMovement3D>()._launchForce = launchForce;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.transform.parent = null;
        }
    }
}
