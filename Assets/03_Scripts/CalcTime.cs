﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcTime : MonoBehaviour
{
    public GameObject Timer;
    [HideInInspector]
    public float _playerTimeAnim;

    void Update()
    {
        Timer.GetComponent<TimeAttack>().playerTimeAnim = _playerTimeAnim;
    }
}
