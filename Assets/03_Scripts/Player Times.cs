﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class PlayerTimes
{
    //public List<float> playerTimes = new List<float>();
    public Dictionary<string, float> times = new Dictionary<string, float>();

    public PlayerTimes(TimeAttack time, string Key)
    {
        if (times.ContainsKey(Key)) {
            times[Key] = time.timer;
        }
        else {
            times.Add(Key, time.timer);
        }
    }
    public void DeleteTimes()
    {
        times.Clear();
    }
}

public static class SaveLoadManager
{
    public static void SaveTime(TimeAttack time, string _Key)
    {
        //Load Current Times
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(Application.persistentDataPath + "/playertimes.time", FileMode.Open);

        PlayerTimes times = (PlayerTimes)bf.Deserialize(file);

        file.Close();

        //Add New Time to File
        bf = new BinaryFormatter();
        file = new FileStream(Application.persistentDataPath + "/playertimes.time", FileMode.Open);

        times.times[_Key] = time.timer;

        bf.Serialize(file, times);
        file.Close();
    }

    public static Dictionary<string, float> LoadTime()
    {
        if (File.Exists(Application.persistentDataPath + "/playertimes.time"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(Application.persistentDataPath + "/playertimes.time", FileMode.Open);

            PlayerTimes times = (PlayerTimes)bf.Deserialize(file);

            file.Close();
            return times.times;
        }
        else {
            Debug.Log("Error Loading File (File Does Not Exist)");
            return null;
        }
    }

    public static void CreateTime(float newTime, string levelName)
    {
        //Load Time Chart
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(Application.persistentDataPath + "/playertimes.time", FileMode.Open);

        PlayerTimes times = (PlayerTimes)bf.Deserialize(file);

        file.Close();

        //Add New Time to Chart & Save
        file = new FileStream(Application.persistentDataPath + "/playertimes.time", FileMode.Open);

        times.times.Add(levelName, newTime);

        bf.Serialize(file, times);
        file.Close();
    }

    public static void DeleteTimes(TimeAttack time)
    {
        //Clear Times
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(Application.persistentDataPath + "/playertimes.time", FileMode.Create);

        //PlayerTimes times = (PlayerTimes)bf.Deserialize(file);
        PlayerTimes times = new PlayerTimes(time, "Blank");
        //times.DeleteTimes();
        //PlayerTimes times = new 

        //times.times = new Dictionary<string, float>();

        bf.Serialize(file, times);
        file.Close();
        Debug.Log("Save Deleted");
    }
}
