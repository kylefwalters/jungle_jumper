﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFOV : MonoBehaviour
{
    public Transform player;
    public float maxAngle;
    public float maxRadius;

    private float angle;
    private Vector2 directionBetween;
    private float angle1;
    private Vector2 directionBetween1;
    private float angle2;
    private Vector2 directionBetween2;
    private float angle3;
    private Vector2 directionBetween3;
    private float angle4;
    private Vector2 directionBetween4;

    public bool isInFOV;

    private Vector2 playerSize = new Vector2(0.35f, 0.35f);
    private Vector2 playerSize2 = new Vector2(0 - 0.35f, 0.35f);

    public LayerMask playerOnly;

    public bool isLooking; //accessed by parent
    Animator anim;

    public void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, maxRadius);

        Vector2 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.forward) * transform.up * maxRadius;
        Vector2 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.forward) * transform.up * maxRadius;

        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, fovLine1);
        Gizmos.DrawRay(transform.position, fovLine2);

        if (!isInFOV)
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, (player.position - transform.position).normalized * maxRadius);
        Gizmos.DrawRay(transform.position, (player.position + (Vector3)playerSize - transform.position).normalized * maxRadius);
        Gizmos.DrawRay(transform.position, (player.position - (Vector3)playerSize - transform.position).normalized * maxRadius);
        Gizmos.DrawRay(transform.position, (player.position + (Vector3)playerSize2 - transform.position).normalized * maxRadius);
        Gizmos.DrawRay(transform.position, (player.position - (Vector3)playerSize2 - transform.position).normalized * maxRadius);

        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, transform.up * maxRadius);
    }

    public static bool inFOV (Transform checkingObject, Transform target, float maxAngle, float maxRadius)
    {
        Collider[] overlaps = new Collider[10];
        Collider2D[] count = Physics2D.OverlapCircleAll(checkingObject.position, maxRadius);
        
        return false;
    }

    public void Update()
    {
        //isInFOV = inFOV(transform, player, maxAngle, maxRadius);
        directionBetween = (player.position - transform.position).normalized;
        angle = Vector2.Angle(transform.up, directionBetween);

        directionBetween1 = (player.position + (Vector3)playerSize - transform.position).normalized;
        angle1 = Vector2.Angle(transform.up, directionBetween1);

        directionBetween2 = (player.position - (Vector3)playerSize - transform.position).normalized;
        angle2 = Vector2.Angle(transform.up, directionBetween2);

        directionBetween3 = (player.position + (Vector3)playerSize2 - transform.position).normalized;
        angle3 = Vector2.Angle(transform.up, directionBetween3);

        directionBetween4 = (player.position - (Vector3)playerSize2 - transform.position).normalized;
        angle4 = Vector2.Angle(transform.up, directionBetween4);

        //Detect Player
        RaycastHit2D hit;
        hit = Physics2D.Raycast(transform.position, player.position - transform.position, maxRadius, playerOnly);
        RaycastHit2D hit1;
        hit1 = Physics2D.Raycast(transform.position, (player.position + (Vector3)playerSize - transform.position).normalized, maxRadius, playerOnly);
        RaycastHit2D hit2;
        hit2 = Physics2D.Raycast(transform.position, (player.position - (Vector3)playerSize - transform.position).normalized, maxRadius, playerOnly);
        RaycastHit2D hit3;
        hit3 = Physics2D.Raycast(transform.position, (player.position + (Vector3)playerSize2 - transform.position).normalized, maxRadius, playerOnly);
        RaycastHit2D hit4;
        hit4 = Physics2D.Raycast(transform.position, (player.position - (Vector3)playerSize2 - transform.position).normalized, maxRadius, playerOnly);

        if(isLooking == true)
        {
            anim.Play("Looking");
        }
        else
        {
            anim.Play("New State");
        }

        if (hit.collider != null && hit.collider.gameObject.tag == "Player" || hit1.collider != null && hit1.collider.gameObject.tag == "Player" || hit2.collider != null && hit2.collider.gameObject.tag == "Player" || hit3.collider != null && hit3.collider.gameObject.tag == "Player" || hit4.collider != null && hit4.collider.gameObject.tag == "Player")
        {
            if (angle <= maxAngle || angle1 <= maxAngle || angle2 <= maxAngle || angle3 <= maxAngle || angle4 <= maxAngle)
            {
                isInFOV = true;
                anim.speed = 0;
                //Player_Health1.phealth -= 0.05f;
                //Player_Health1.phealth = Mathf.Lerp(0, 1, Mathf.PingPong(Time.time, 0));
            }
            else
            {
                isInFOV = false;
                anim.speed = 1;
            }
        }
        else
        {
            isInFOV = false;
            anim.speed = 1;
        }
    }
}
