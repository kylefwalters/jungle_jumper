﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject profiles;
    public GameObject options;

    private GameObject currentMenu;
    private GameObject lastMenu;

    [Header("Sound Effects")]
    public AudioSource press;
    public AudioSource back;

    public void Play()
    {
        press.Play();

        mainMenu.SetActive(!mainMenu.activeSelf);
        profiles.SetActive(!profiles.activeSelf);

        if (mainMenu.activeSelf)
        {
            currentMenu = mainMenu;
            lastMenu = profiles;
        }
        else
        {
            currentMenu = profiles;
            lastMenu = mainMenu;
        }
    }

    public void Options()
    {
        press.Play();

        if (mainMenu.activeSelf)
        {
            currentMenu = options;
            lastMenu = mainMenu;

            mainMenu.SetActive(!mainMenu.activeSelf);
            options.SetActive(!options.activeSelf);
        }
        else
        {
            currentMenu = mainMenu;
            lastMenu = options;

            options.GetComponent<Animator>().Play("OptionsMM Close");
            StartCoroutine(_Options());
        }
    }
    IEnumerator _Options()
    {
        yield return new WaitForSeconds(0.1f);
        Animator animatorOptions = options.GetComponent<Animator>();
        while (animatorOptions.GetCurrentAnimatorStateInfo(0).normalizedTime <= 1.0f) { 
            yield return new WaitForSeconds(0.05f);
        }

        mainMenu.SetActive(!mainMenu.activeSelf);
        options.SetActive(!options.activeSelf);
        yield return null;
    }

    public void LoadLevel(string sceneName)
    {
        press.Play();

        SceneManager.LoadScene(sceneName);
    }

    public void Back()
    {
        press.Play();

        currentMenu.SetActive(false);
        lastMenu.SetActive(true);

        GameObject _currentMenu = currentMenu;
        currentMenu = lastMenu;
        lastMenu = _currentMenu;
    }

    public void ExitGame()
    {
        press.Play();

        Application.Quit();
    }
}
