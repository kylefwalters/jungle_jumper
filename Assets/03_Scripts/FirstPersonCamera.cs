﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCamera : MonoBehaviour
{
    Vector2 rotation = Vector2.zero;
    public float lookSensitivity = 4.8f;

    void Start()
    {
        //Hide Cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

    }

    void Update()
    {
        //Camera Rotation
        rotation.y += Input.GetAxis("Mouse X");
        rotation.x -= Input.GetAxis("Mouse Y");
        rotation.x = Mathf.Clamp(rotation.x, -18f, 22f); //Limit Camera Up/Down Movement

        transform.parent.eulerAngles = new Vector2(0, rotation.y) * lookSensitivity;
        transform.localRotation = Quaternion.Euler(rotation.x * lookSensitivity, 0, 0);
    }

    private void OnDrawGizmos()
    {
        
    }
}
