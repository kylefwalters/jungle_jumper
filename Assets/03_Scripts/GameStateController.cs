﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameStateController : MonoBehaviour
{
    public KeyCode pauseKey = KeyCode.Escape;

    private enum states
    {
        play,
        pause
    }
    private states currentState = states.play;

    public int nextLevelIndex;

    [Header("GameObejcts")]
    public GameObject pauseMenu;
    public GameObject optionsMenu;
    public GameObject timer;
    private GameObject player;
    private GameObject[] movingPlatforms;

    [Header("Sound Effects")]
    public AudioSource uiPress;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        movingPlatforms = GameObject.FindGameObjectsWithTag("Moving");
    }

    void Update()
    {
        ///Pause & Unpause
        if (Input.GetKeyDown(pauseKey))
        {
            Pause();
        }
    }

    public void Pause()
    {
        if (currentState == states.play)
        {
            ///Play Pause SE

            currentState = states.pause;
            player.GetComponent<PlayerMovement3D>().Pause();
            player.transform.GetChild(0).gameObject.GetComponent<FirstPersonCamera>().enabled = false;
            player.transform.GetChild(0).gameObject.GetComponent<PlatformSpawner>().Pause();

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            pauseMenu.SetActive(true);
            timer.GetComponent<TimeAttack>().enabled = false;
        }
        else if (currentState == states.pause && !optionsMenu.activeSelf)
        {
            uiPress.Play();

            currentState = states.play;
            player.GetComponent<PlayerMovement3D>().Pause();
            player.transform.GetChild(0).gameObject.GetComponent<FirstPersonCamera>().enabled = true;
            player.transform.GetChild(0).gameObject.GetComponent<PlatformSpawner>().enabled = true;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;


            pauseMenu.SetActive(false);
            timer.GetComponent<TimeAttack>().enabled = true;
        }
    }

    public void Finish()
    {
        player.GetComponent<PlayerMovement3D>().Pause();
        player.transform.GetChild(0).gameObject.GetComponent<FirstPersonCamera>().enabled = false;
        player.transform.GetChild(0).gameObject.GetComponent<PlatformSpawner>().Pause();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        timer.GetComponent<TimeAttack>().enabled = false;
        timer.GetComponent<TextMeshProUGUI>().enabled = false;
    }

    public void Restart()
    {
        uiPress.Play();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Options()
    {
        uiPress.Play();


    }

    public void MainMenu()
    {
        uiPress.Play();

        SceneManager.LoadScene("Main Menu");
    }

    public void Continue()
    {
        SceneManager.LoadScene(nextLevelIndex);
    }

    public void Exit()
    {
        uiPress.Play();

        Application.Quit();
    }
}
