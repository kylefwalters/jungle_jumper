﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement3D : MonoBehaviour
{
    Rigidbody rb;
    CharacterController controller;
    public LayerMask targetableLayers;
    bool canJump = false;
    [Header("Movement Attributes")]
    public float jumpStrength = 6.0f;
    float jumpForce = 0;
    public float gravity = 12.5f;
    public float moveSpeed = 5.0f;
    [HideInInspector]
    public Vector3 _launchForce;
    private Vector3 currentForce;
    private bool isVClip;

    [Header("Sound Effects")]
    public AudioSource jump;
    public AudioSource land;

    private enum states
    {
        play,
        pause,
        vclip
    }
    private states currentState = states.play;
    private Vector3 storedVelocity;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        //Play/Pause
        if (currentState == states.play) { }
        else if (currentState == states.pause) { return; }
        else if (currentState == states.vclip) {
            VClip();
            return;
        }

        //Jump
        RaycastHit hit;
        /*RaycastHit hitTR;
        RaycastHit hitBR;
        RaycastHit hitBL;
        RaycastHit hitTL;
        RaycastHit hit2;
        RaycastHit hit3;
        RaycastHit hit4;
        RaycastHit hit5;*/
        Vector3 extents = gameObject.GetComponent<BoxCollider>().bounds.extents;
        Vector3 center = gameObject.GetComponent<BoxCollider>().center;
        bool m_hit = Physics.BoxCast(transform.position + new Vector3(0,0.15f,0), new Vector3(extents.x, 0.01f, extents.z), Vector3.down, out hit, transform.rotation,  1<<targetableLayers);
        /*Physics.Raycast(transform.position - new Vector3(0, 0.8f, 0), Vector3.down, out hit, 0.05f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.8f - 0.4f, 0) + new Vector3(extents.x, 0, extents.z), Vector3.down, out hitTR, 0.45f, targetableLayers); ///Raycast inside of player for slopes
        Physics.Raycast(transform.position - new Vector3(0, 0.8f - 0.4f, 0) + new Vector3(extents.x, 0, -extents.z), Vector3.down, out hitBR, 0.45f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.8f - 0.4f, 0) + new Vector3(-extents.x, 0, -extents.z), Vector3.down, out hitBL, 0.45f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.8f - 0.4f, 0) + new Vector3(-extents.x, 0, extents.z), Vector3.down, out hitTL, 0.45f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.4f, 0) + new Vector3(extents.x / 2, 0, extents.z / 2), Vector3.down, out hit2, 0.45f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.4f, 0) + new Vector3(extents.x / 2, 0, -extents.z / 2), Vector3.down, out hit3, 0.45f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.4f, 0) + new Vector3(-extents.x / 2, 0, -extents.z / 2), Vector3.down, out hit4, 0.45f, targetableLayers);
        Physics.Raycast(transform.position - new Vector3(0, 0.4f, 0) + new Vector3(-extents.x / 2, 0, extents.z / 2), Vector3.down, out hit5, 0.45f, targetableLayers);*/

        if (hit.collider != null)
        {
            if (!canJump)
            {
                land.Play();
            }
            canJump = true;
        } else { canJump = false; }

        if (Input.GetKeyDown(KeyCode.Space) && canJump)
        {
            jump.Play();
            jumpForce = jumpStrength; ///make jump strength be affected by length jump button is held
        }
        if(jumpForce > 0)
            jumpForce += _launchForce.y * 0.4f;
        else
            jumpForce += _launchForce.y;
        jumpForce -= (gravity * Time.deltaTime);

        if (canJump && jumpForce <= 0)
        {
            jumpForce = 0;
        }
        jumpForce = Mathf.Clamp(jumpForce, -15f, Mathf.Infinity);

        //Move
        Vector3 movementDir = Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up) * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        _launchForce = new Vector3(_launchForce.x, 0, _launchForce.z);
        if (canJump) { _launchForce = Vector3.SmoothDamp(_launchForce, Vector3.zero, ref currentForce, .5f); }
        else { _launchForce = Vector3.SmoothDamp(_launchForce, Vector3.zero, ref currentForce, .9f); }
        //Compare Horizontal Axis
        if(Mathf.Sign(movementDir.x) != Mathf.Sign(_launchForce.x))
        { _launchForce.x += (movementDir.x / 10 * Time.deltaTime); }
        //Compare Vertical Axis
        if (Mathf.Sign(movementDir.z) != Mathf.Sign(_launchForce.z))
        { _launchForce.z += (movementDir.z / 10 * Time.deltaTime); }
        controller.Move((movementDir * moveSpeed + new Vector3(0, jumpForce, 0) + _launchForce) * Time.deltaTime);

        #region DevCommands
        //VClip
        if (Input.GetKeyDown(KeyCode.V) && currentState == states.play) {
            isVClip = true;
            currentState = states.vclip;
        }else if(Input.GetKeyDown(KeyCode.V) && currentState == states.vclip)
            currentState = states.play;
        #endregion
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Death")
        {
            GameOver();
        }
    }

    private void VClip()
    {
        int verticalMove = 0;
        if (Input.GetKey(KeyCode.Space))
            verticalMove = 4;
        else if (Input.GetKey(KeyCode.C))
            verticalMove = -4;

        Vector3 moveStrength = new Vector3(Input.GetAxis("Horizontal") * 6f, verticalMove, Input.GetAxis("Vertical") * 6f);
        Transform lookDir = Camera.main.transform;
        controller.Move(Quaternion.LookRotation(lookDir.forward, lookDir.up) * moveStrength * Time.deltaTime);
    }

    ///Puase & Unpause Function
    public void Pause()
    {
        switch (currentState)
        {
            case states.play:
                currentState = states.pause;
                storedVelocity = controller.velocity;
                break;
            case states.pause:
                if(isVClip)
                    currentState = states.vclip;
                else
                    currentState = states.play;
                break;
            case states.vclip:
                currentState = states.pause;
                storedVelocity = controller.velocity;
                break;
        }
    }

    void GameOver()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    private void OnDisable()
    {
        Debug.Log(UnityEngine.StackTraceUtility.ExtractStackTrace());
    }
}
