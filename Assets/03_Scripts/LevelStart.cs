﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//For Level_Start Animation
public class LevelStart : MonoBehaviour
{
    //[HideInInspector] //The Height of the object relative to their original position
    public float animHeight;
    Animator anim;
    float origHeight;
    public float bottomHeight = -70.0f;

    public List<GameObject> children = new List<GameObject>();

    void Start()
    {
        anim = GetComponent<Animator>();
        origHeight = transform.position.y;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.name == "Start Collider") { }
            else
                children.Add(transform.GetChild(i).gameObject);
        }
        //Hide Children at Start
        foreach (GameObject child in children)
            child.SetActive(false);
    }

    void Update()
    {
        float currentHeight = Mathf.Lerp(bottomHeight, origHeight, animHeight);
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

        if (animHeight == 1.0f)
        {
            this.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        anim.Play("Level_Start");

        //Activate Children; Makes Object Visible
        foreach (GameObject child in children)
            child.SetActive(true);
    }
}
