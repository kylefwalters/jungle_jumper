﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

//Attatch to Level Goal
//Text must be TextMeshPro
public class TimeAttack : MonoBehaviour
{
    [Tooltip("Timer object in scene")]
    public GameObject Timer;
    private TextMeshProUGUI text;
    //Player's Time
    [HideInInspector]
    public float timer;
    [Tooltip("Time limit to earn turbo status clear")]
    public float turboTime = 20.0f;
    [Header("Results")]
    public GameObject resultsScreen;
    public GameObject gameStateController;
    [Tooltip("The text display for the player's time")]
    public GameObject playerTime;
    //Player's Best Time; loaded from playertimes.time
    [Tooltip("Object in scene that tracks the Player's top time")]
    public GameObject bestTimer;
    [HideInInspector]
    public float bestTime;
    //Float for animating player time in results; 
    [HideInInspector]
    public float playerTimeAnim = 0.0f;

    [Header("Buttons")]
    public GameObject continueButton;
    public GameObject restartButton;
    public GameObject mainMenuButton;

    [Header("Sound Effects"), Tooltip("'Tick' sound that plays while counting the Player's time in the Results Screen")]
    public AudioSource tick;
    [Range(0.03f, 0.1f), Tooltip("The frequency of ticks; also the total number of times the tick sound plays")]
    public float tickFrequency;

    //Won't Update Best Time if true
    bool cheated;

    void Start()
    {
        //Start tracking when Timer is enabled
        //Can keep timer disabled to pause time or delay timer start
        text = Timer.GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        //Measure time & update display
        #region Timer
        timer += Time.deltaTime;
        int minutes = Mathf.FloorToInt(timer) / 60;
        timer = timer % 60;
        text.text = string.Format("{0}{1:#:00.00}s", minutes, timer);
        timer += minutes * 60;
        #endregion

        //Check if VClip was used
        if (Input.GetKeyDown(KeyCode.V))
            cheated = true;
    }

    public void Finish()
    {
        resultsScreen.SetActive(true);

        if (File.Exists(Application.persistentDataPath + "/playertimes.time"))
        {
            Load();
        }
        else
            Save();

        StartCoroutine(_Finish());
    }
    IEnumerator _Finish()
    {
        float _timer = timer;
        gameStateController.GetComponent<GameStateController>().Finish();
        TextMeshProUGUI pTimeText = playerTime.GetComponent<TextMeshProUGUI>();

        float _tickFrequency = tickFrequency;
        float _playerTimeAnim = 0.0f;
        float lastPlayerTimeAnim = 0.0f;

        while (playerTimeAnim < 1.0f)
        {
            //Play "Tick" Sound
            _tickFrequency -= _playerTimeAnim;
            if(_tickFrequency <= 0)
            {
                tick.Play();
                _tickFrequency = tickFrequency; }
            //Change in playerTimeAnim
            _playerTimeAnim = playerTimeAnim - lastPlayerTimeAnim;
            lastPlayerTimeAnim = playerTimeAnim;

            _timer = timer * playerTimeAnim;
            int minutes = Mathf.FloorToInt(_timer) / 60;
            _timer = _timer % 60;
            if(minutes == 0)
                pTimeText.text = string.Format("{0:#00.00}s", _timer);
            else
                pTimeText.text = string.Format("{0}{1:#:00.00}s", minutes, _timer);
            _timer += minutes * 60;
            yield return new WaitForEndOfFrame();
        }
        tick.Play();
        GameObject grade = resultsScreen.transform.GetChild(7).gameObject;
        TextMeshProUGUI gradeText = grade.GetComponent<TextMeshProUGUI>();
        grade.SetActive(true);
        if(timer > turboTime)
            gradeText.text = "Level Clear!";
        else
        {
            gradeText.text = "Turbo Clear!";
            gradeText.color = new Color(30, 144, 255);
            gradeText.fontStyle = FontStyles.Bold;
        }
        yield return new WaitForSeconds(1.0f);
        continueButton.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        restartButton.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        mainMenuButton.SetActive(true);
        yield return null;
    }

    //Saves Time to PlayerTime.cs if it beats the current Best Time
    public void Save()
    {
        SaveLoadManager.SaveTime(this, SceneManager.GetActiveScene().name);
        Debug.Log("Best Time Updated");
    }

    //Loads Current Best Time from PlayerTime.cs
    public void Load()
    {
        Dictionary<string, float> times = SaveLoadManager.LoadTime();

        //Check that Best Time for Current Level Exists
        if (times.ContainsKey(SceneManager.GetActiveScene().name))
        {
            //print("Loading Time...");
            bestTime = times[SceneManager.GetActiveScene().name];
            //print("Current Record: " + bestTime);

            //Update UI Display
            float _bestTime = bestTime;
            int minutes = Mathf.FloorToInt(_bestTime) / 60;
            _bestTime = _bestTime % 60;
            TextMeshProUGUI bTimeText = bestTimer.GetComponent<TextMeshProUGUI>();
            if (minutes == 0)
                bTimeText.text = string.Format("{0:#00.00}s", _bestTime);
            else
                bTimeText.text = string.Format("{0}{1:#:00.00}s", minutes, _bestTime);

            //Update Best Time if a New Record is Set & No Cheats Were Used
            if (timer < bestTime && !cheated)
            {
                Save();
            }
            //print("Done");
        }
        else
        {
            print("Key not detected, Creating Key");
            SaveLoadManager.CreateTime(bestTime, SceneManager.GetActiveScene().name);

            float _bestTime = bestTime;
            int minutes = Mathf.FloorToInt(_bestTime) / 60;
            _bestTime = _bestTime % 60;
            TextMeshProUGUI bTimeText = bestTimer.GetComponent<TextMeshProUGUI>();
            if (minutes == 0)
                bTimeText.text = string.Format("{0:#00.00}s", _bestTime);
            else
                bTimeText.text = string.Format("{0}{1:#:00.00}s", minutes, _bestTime);

            Save();
        }
    }

    public void DeleteSave()
    {
        SaveLoadManager.DeleteTimes(this);
    }
}

